import { BadRequestException } from '@nestjs/common';

export interface QueryRaw {
  select?: string | string[];
  filter?: string | string[];
  or?: string | string[];
  sort?: string | string[];
  page?: string;
  limit?: string;
}

const queryFilterConditions = [
  'eq',
  'ne',
  'gt',
  'lt',
  'gte',
  'lte',
  'cont',
  'excl',
];
type QueryFilterCondition =
  | 'eq'
  | 'ne'
  | 'gt'
  | 'lt'
  | 'gte'
  | 'lte'
  | 'cont'
  | 'excl';
// other: "starts" | "ends" | "in" | "notin" | "isnull" | "notnull" | "between"
type QueryOrCondition = QueryFilterCondition;

interface QueryFilter {
  field: string;
  condition: QueryFilterCondition;
  value: string | number;
}

// tslint:disable-next-line:no-empty-interface
interface QueryOr extends QueryFilter {}

interface QuerySort {
  field: string;
  order: 'ASC' | 'DESC';
}

export class QueryBase {
  public selects: string[];
  public filters: QueryFilter[];
  public ors: QueryOr[];
  public sorts: QuerySort[];
  public page: number;
  public limit: number;

  private selectableFields: string[];

  public static alwaysShownFields: string[] = [
    'id',
    'created_at',
    'updated_at',
    'deleted_at',
  ];
  public static selectableFieldsBase: string[] = [
    ...QueryBase.alwaysShownFields,
    'name',
    'code',
    'is_closed',
  ];

  constructor(item: QueryRaw, selectableFields: string[]) {
    this.selects = this.initSelects(item);
    this.filters = this.initFilters(item);
    this.ors = this.initOrs(item);
    this.sorts = this.initSorts(item);
    this.page = this.initPage(item);
    this.limit = this.initLimit(item);
    this.selectableFields = [
      ...QueryBase.selectableFieldsBase,
      ...selectableFields,
    ];
    this.validateFields();
  }

  private initSelects(item: QueryRaw): string[] {
    if (item.select === undefined) {
      return [];
    }
    if (item.select === '') {
      return [];
    }
    if (Array.isArray(item.select)) {
      throw new BadRequestException();
    }
    return item.select.split(',');
  }

  private initFilters(item: QueryRaw): QueryFilter[] {
    if (item.filter === undefined) {
      return [];
    }
    if (item.filter === '') {
      return [];
    }
    const filterRaws: string[] = Array.isArray(item.filter)
      ? item.filter
      : [item.filter];
    const filters: QueryFilter[] = [];
    filterRaws.forEach((filterRaw: string) => {
      const filterComponents: string[] = filterRaw.split('||');
      if (filterComponents.length !== 3) {
        throw new BadRequestException();
      }
      if (queryFilterConditions.indexOf(filterComponents[1]) === -1) {
        throw new BadRequestException();
      }
      filters.push({
        field: filterComponents[0],
        condition: filterComponents[1] as QueryFilterCondition,
        value: isNaN(Number(filterComponents[2]))
          ? filterComponents[2]
          : Number(filterComponents[2]),
      });
    });
    return filters;
  }

  private initOrs(item: QueryRaw): QueryOr[] {
    if (item.or === undefined) {
      return [];
    }
    if (item.or === '') {
      return [];
    }
    const orRaws: string[] = Array.isArray(item.or) ? item.or : [item.or];
    const ors: QueryOr[] = [];
    orRaws.forEach((orRaw: string) => {
      const orComponents: string[] = orRaw.split('||');
      if (orComponents.length !== 3) {
        throw new BadRequestException();
      }
      if (queryFilterConditions.indexOf(orComponents[1]) === -1) {
        throw new BadRequestException();
      }
      ors.push({
        field: orComponents[0],
        condition: orComponents[1] as QueryOrCondition,
        value: isNaN(Number(orComponents[2]))
          ? orComponents[2]
          : Number(orComponents[2]),
      });
    });
    return ors;
  }

  private initSorts(item: QueryRaw): QuerySort[] {
    if (item.sort === undefined) {
      return [];
    }
    if (item.sort === '') {
      return [];
    }
    const sortRaws: string[] = Array.isArray(item.sort)
      ? item.sort
      : [item.sort];
    const sorts: QuerySort[] = [];
    sortRaws.forEach((sortRaw: string) => {
      const sortComponents: string[] = sortRaw.split(',');
      if (sortComponents.length !== 2) {
        throw new BadRequestException();
      }
      if (['ASC', 'DESC'].indexOf(sortComponents[1]) === -1) {
        throw new BadRequestException();
      }
      sorts.push({
        field: sortComponents[0],
        order: sortComponents[1] === 'ASC' ? 'ASC' : 'DESC',
      });
    });
    return sorts;
  }

  private initPage(item: QueryRaw): number {
    if (item.page === undefined) {
      return 1; // default
    }
    if (item.page === '') {
      return 1; // default
    }
    if (Array.isArray(item.page)) {
      throw new BadRequestException();
    }
    if (!Number.isInteger(Number(item.page))) {
      throw new BadRequestException();
    }
    if (Number(item.page) < 0) {
      throw new BadRequestException();
    }
    return Number(item.page);
  }

  private initLimit(item: QueryRaw): number {
    if (item.limit === undefined) {
      return 0;
    }
    if (item.limit === '') {
      return 0;
    }
    if (Array.isArray(item.limit)) {
      throw new BadRequestException();
    }
    if (!Number.isInteger(Number(item.limit))) {
      throw new BadRequestException();
    }
    if (Number(item.limit) < 0) {
      throw new BadRequestException();
    }
    return Number(item.limit);
  }

  private validateFields(): void {
    if (this.selects.length === 0) {
      this.selects = this.selectableFields;
    } else {
      this.selects.forEach((select: string) => {
        if (this.selectableFields.indexOf(select) === -1) {
          throw new BadRequestException();
        }
      });
      this.selects = [...this.selects, ...QueryBase.alwaysShownFields];
      this.selects = Array.from(new Set(this.selects));
    }
    if (this.filters.length > 0) {
      this.filters.forEach((filter: QueryFilter) => {
        if (this.selectableFields.indexOf(filter.field) === -1) {
          throw new BadRequestException();
        }
      });
    }
    if (this.ors.length > 0) {
      this.ors.forEach((or: QueryFilter) => {
        if (this.selectableFields.indexOf(or.field) === -1) {
          throw new BadRequestException();
        }
      });
    }
  }
}

interface FilterBase {
  [key: string]: string | number;
}
interface MFilterBase {
  [key: string]: MFilterBase[] | FilterBase | undefined;
}

export function buildMQuery<T extends QueryBase>(query: T) {
  // c.f. https://github.com/nestjsx/crud/wiki/Requests#query-params
  const mFilter: MFilterBase = {};
  const mSelect: string = query.selects.join(' ');
  if (query.filters.length === 0) {
    if (query.ors.length === 1) {
      // filter: 0, or: 1
      mFilter[query.ors[0].field] = {
        [`$${query.ors[0].condition}`]: query.ors[0].value,
      };
    } else if (query.ors.length > 1) {
      // filter: 0, or: many
      mFilter.$or = query.ors.map((or: QueryOr) => {
        return {
          [or.field]: {
            [`$${or.condition}`]: or.value,
          },
        };
      });
    }
  } else if (query.filters.length === 1) {
    if (query.ors.length === 0) {
      // filter: 1, or: 0
      mFilter[query.filters[0].field] = {
        [`$${query.filters[0].condition}`]: query.filters[0].value,
      };
    } else if (query.ors.length === 1) {
      // filter: 1, or: 1
      mFilter.$or = [
        {
          [query.filters[0].field]: {
            [`$${query.filters[0].condition}`]: query.filters[0].value,
          },
        },
        {
          [query.ors[0].field]: {
            [`$${query.ors[0].condition}`]: query.ors[0].value,
          },
        },
      ];
    } else {
      // filter: 1, or: many
      mFilter.$or = [
        {
          [query.filters[0].field]: {
            [`$${query.filters[0].condition}`]: query.filters[0].value,
          },
        },
        {
          $and: query.ors.map((or: QueryOr) => {
            return {
              [or.field]: {
                [`$${or.condition}`]: or.value,
              },
            };
          }),
        },
      ];
    }
  } else {
    if (query.ors.length === 0) {
      // filter: many, or: 0
      mFilter.$and = query.filters.map((filter: QueryFilter) => {
        return {
          [filter.field]: {
            [`$${filter.condition}`]: filter.value,
          },
        };
      });
    } else if (query.ors.length === 1) {
      // filter: many, or: 1
      mFilter.$or = [
        {
          $and: query.filters.map((filter: QueryFilter) => {
            return {
              [filter.field]: {
                [`$${filter.condition}`]: filter.value,
              },
            };
          }),
        },
        {
          [query.ors[0].field]: {
            [`$${query.ors[0].condition}`]: query.ors[0].value,
          },
        },
      ];
    } else {
      // filter: many, or: many
      mFilter.$or = [
        {
          $and: query.filters.map((filter: QueryFilter) => {
            return {
              [filter.field]: {
                [`$${filter.condition}`]: filter.value,
              },
            };
          }),
        },
        {
          $and: query.ors.map((or: QueryOr) => {
            return {
              [or.field]: {
                [`$${or.condition}`]: or.value,
              },
            };
          }),
        },
      ];
    }
  }
  const mSort: {
    [key: string]: 1 | -1;
  } = {};
  query.sorts.forEach((sort: QuerySort) => {
    mSort[sort.field] = sort.order === 'ASC' ? 1 : -1;
  });
  const mOptions = {
    sort: mSort,
    skip: (query.page - 1) * query.limit,
    limit: query.limit,
  };
  return {
    mFilter,
    mSelect,
    mOptions,
  };
}
