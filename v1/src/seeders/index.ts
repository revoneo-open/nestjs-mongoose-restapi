import mongoose from 'mongoose';

import { mongodb } from '../app.constants';
import { createUsers } from './users.seeder';

mongoose.connect(mongodb, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

(async () => {
  try {
    await createUsers();
  } catch (e) {
    console.error(e);
  } finally {
    mongoose.disconnect();
  }
})();
