import mongoose from 'mongoose';

import { UserDocument, UserSchema, CreateUserDto } from '../users/users.entity';
import { UsersService } from '../users/users.service';
import { getHash } from '../auth/auth.bcrypt';

const UserModel = mongoose.model<UserDocument>('User', UserSchema);

const data = async (): Promise<CreateUserDto[]> => [
  {
    name: 'name',
    code: 'code',
    is_closed: false,
    is_admin: true,
    can_view_limited: true,
    password: await getHash('password123'),
  },
];

const shouldRun = async (): Promise<boolean> => {
  await UserModel.deleteMany({}).exec();
  return (await new UsersService(UserModel).countAll()) === 0;
};

export const createUsers = async () => {
  if (await shouldRun()) {
    await new UsersService(UserModel).createBulk(await data());
  }
};
