export const mongodb = process.env.MONGODB || '';

export const jwtConstants = {
  secret: 'secretKey',
  expiresIn: '24h',
};

export const apiDecorators = {
  implicitParam: {
    id: { name: 'id', required: false },
  },
  implicitQuery: {
    limit: { name: 'limit', required: false },
    page: { name: 'page', required: false },
    sort: { name: 'sort', required: false },
    or: { name: 'or', required: false },
    filter: { name: 'filter', required: false },
    select: { name: 'select', required: false },
    filterIds: {
      name: 'filter',
      required: false,
      description: '{"ids":["[id1]","[id2]",...]}',
    },
  },
};
