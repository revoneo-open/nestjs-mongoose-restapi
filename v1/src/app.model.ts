import {
  NotFoundException,
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Model, Document, Schema } from 'mongoose';

import { EntityBase, DateWise } from './app.entity';
import { QueryBase, buildMQuery } from './app.query';

export async function create<T extends EntityBase, U extends Document>(
  createDto: T,
  model: Model<U>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  try {
    const createdItem = new model(createDto);
    const created = await createdItem.save();
    return findOne(created._id, model, { populates });
  } catch (e) {
    const errors: string[] = [];
    for (const error of Object.keys(e.errors)) {
      errors.push(e.errors[error].message);
    }
    throw new BadRequestException(errors.join('\n'));
  }
}

export async function createBulk<T extends EntityBase, U extends Document>(
  createDtos: T[],
  model: Model<U>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  try {
    const createds = await Promise.all(
      createDtos.map(async (createDto: T) => {
        const createdItem = new model(createDto);
        return await createdItem.save();
      }),
    );
    let documentQuery = model.find({
      _id: {
        $in: createds.map(created => created._id),
      },
    });
    populates.forEach((populate: string) => {
      documentQuery = documentQuery.populate(populate);
    });
    return await documentQuery.exec();
  } catch (e) {
    const errors: string[] = [];
    for (const error of Object.keys(e.errors)) {
      errors.push(e.errors[error].message);
    }
    throw new BadRequestException(errors.join('\n'));
  }
}

export async function findAll<T extends QueryBase, U extends Document>(
  query: T,
  model: Model<U>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  const { mFilter, mSelect, mOptions } = buildMQuery<T>(query);
  let documentQuery = model.find(mFilter, mSelect, mOptions);
  populates.forEach((populate: string) => {
    documentQuery = documentQuery.populate(populate);
  });
  try {
    return await documentQuery.exec();
  } catch (e) {
    throw new InternalServerErrorException();
  }
}

export async function countAll<T extends QueryBase, U extends Document>(
  query: T,
  model: Model<U>,
) {
  const { mFilter } = buildMQuery<T>(query);
  try {
    return await model.countDocuments(mFilter).exec();
  } catch (e) {
    throw new InternalServerErrorException();
  }
}

export async function findOne<T extends Document>(
  id: Schema.Types.ObjectId,
  model: Model<T>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  let item: T | null;
  try {
    let documentQuery = model.findById(id);
    populates.forEach((populate: string) => {
      documentQuery = documentQuery.populate(populate);
    });
    item = await documentQuery.exec();
  } catch (e) {
    if (e.kind === 'ObjectId') {
      throw new NotFoundException();
    }
    throw new InternalServerErrorException();
  }
  if (item === null) {
    throw new NotFoundException();
  }
  return item;
}

export async function update<T, U extends Document>(
  id: Schema.Types.ObjectId,
  updateDto: T,
  model: Model<U>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  let item: U | null;
  try {
    let documentQuery = model.findByIdAndUpdate(id, updateDto, {
      new: true,
      runValidators: true,
      context: 'query',
    });
    populates.forEach((populate: string) => {
      documentQuery = documentQuery.populate(populate);
    });
    item = await documentQuery.exec();
  } catch (e) {
    if (e.kind === 'ObjectId') {
      throw new NotFoundException();
    }
    if (e.name === 'CastError') {
      throw new BadRequestException(e.message);
    }
    if (e.name === 'ValidationError') {
      throw new BadRequestException(e.message);
    }
    throw new InternalServerErrorException();
  }
  if (item === null) {
    throw new NotFoundException();
  }
  return item;
}

export async function updateMany<T, U extends Document>(
  filter: string | { ids: Schema.Types.ObjectId[] },
  updateDto: T,
  model: Model<U>,
) {
  if (typeof filter === 'string') {
    filter = JSON.parse(filter) as { ids: Schema.Types.ObjectId[] };
  }
  if (!Array.isArray(filter.ids) || filter.ids.length === 0) {
    throw new BadRequestException();
  }
  try {
    await model
      .updateMany(
        {
          _id: {
            $in: filter.ids,
          },
        },
        updateDto,
        {
          new: true,
          runValidators: true,
          context: 'query',
        },
      )
      .exec();
  } catch (e) {
    if (e.kind === 'ObjectId') {
      throw new NotFoundException();
    }
    if (e.name === 'CastError') {
      throw new BadRequestException(e.message);
    }
    if (e.name === 'ValidationError') {
      throw new BadRequestException(e.message);
    }
    throw new InternalServerErrorException();
  }
  return filter.ids;
}

export async function softDelete<T extends Document>(
  id: Schema.Types.ObjectId,
  model: Model<T>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  return await update<Partial<DateWise>, T>(
    id,
    { deleted_at: new Date() },
    model,
    { populates },
  );
}

export async function softDeleteMany<T extends Document>(
  filter: { ids: Schema.Types.ObjectId[] },
  model: Model<T>,
) {
  return await updateMany<Partial<DateWise>, T>(
    filter,
    { deleted_at: new Date() },
    model,
  );
}

export async function hardDelete<T extends Document>(
  id: Schema.Types.ObjectId,
  model: Model<T>,
  { populates }: { populates: string[] } = { populates: [] },
) {
  let item: T | null;
  try {
    let documentQuery = model.findByIdAndDelete(id);
    populates.forEach((populate: string) => {
      documentQuery = documentQuery.populate(populate);
    });
    item = await documentQuery.exec();
  } catch (e) {
    if (e.kind === 'ObjectId') {
      throw new NotFoundException();
    }
    throw new InternalServerErrorException();
  }
  if (item === null) {
    throw new NotFoundException();
  }
  return item;
}
