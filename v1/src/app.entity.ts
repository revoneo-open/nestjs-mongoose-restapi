import { ApiModelProperty } from '@nestjs/swagger';
import { Document, Schema } from 'mongoose';
import { Exclude } from 'class-transformer';

export interface EntityBase {
  name: string;
  code: string;
  is_closed: boolean;
}

export interface DateWise {
  deleted_at: Date;
  created_at: Date;
  updated_at: Date;
}

export class ResponseBase<T extends Document & DateWise> {
  @ApiModelProperty()
  id: string;

  @Exclude()
  _id: Schema.Types.ObjectId = new Schema.Types.ObjectId('');

  @Exclude()
  __v: number = 0;

  @ApiModelProperty()
  name?: string;

  @ApiModelProperty()
  code?: string;

  @ApiModelProperty()
  is_closed?: boolean;

  @ApiModelProperty()
  deleted_at: number;

  @ApiModelProperty()
  created_at: number;

  @ApiModelProperty()
  updated_at: number;

  constructor(item: T & { [prop: string]: any }) {
    item = item.toJSON();
    Object.assign(this, item);
    this.id = String(item._id);
    this.created_at = item.created_at.getTime();
    this.updated_at = item.updated_at.getTime();
    this.deleted_at = item.deleted_at === null ? 0 : item.deleted_at.getTime(); // default null -> 0
  }
}

export class ResponseIdsDto {
  @ApiModelProperty()
  data: {
    ids: Schema.Types.ObjectId[];
  };

  constructor(ids: Schema.Types.ObjectId[]) {
    this.data = { ids };
  }
}

export class ResponseDeletedDto<T extends Document & DateWise> {
  @ApiModelProperty()
  id: string;

  constructor(item: T) {
    this.id = item._id.toString();
  }
}

export const nameSchema = {
  name: {
    type: Schema.Types.String,
    validate: {
      validator: (v: string) => v.length >= 4,
      message: '4文字以上に設定してください',
    } as any,
    required: true,
    unique: true,
  },
};

export const codeSchema = {
  code: {
    type: Schema.Types.String,
    validate: {
      validator: (v: string) => v.length >= 4,
      message: '4文字以上に設定してください',
    } as any,
    required: true,
    unique: true,
  },
};

export const isClosedSchema = {
  is_closed: {
    type: Schema.Types.Boolean,
    default: false,
  },
};

export const deletedAtSchema = {
  deleted_at: { type: Schema.Types.Date, default: null },
};

export const timestamps = {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
};
