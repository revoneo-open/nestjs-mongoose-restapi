import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import contextService from 'request-context';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    // i18n
    contextService.set('i18n', request.__);
    // role
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    contextService.set('roles', roles);
    return true;
  }
}
