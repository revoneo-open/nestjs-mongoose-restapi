import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

import { getHash } from '../auth/auth.bcrypt';
import { CreateUserDto, UpdateUserDto } from '../users/users.entity';

type UserDto = CreateUserDto | UpdateUserDto;
type UserDtos = UserDto | UserDto[];

@Injectable()
export class PasswordHashPipe
  implements PipeTransform<UserDtos, Promise<UserDtos>> {
  async transform(
    userDtos: UserDtos,
    _metadata: ArgumentMetadata,
  ): Promise<UserDtos> {
    let isArray = true;
    if (!Array.isArray(userDtos)) {
      isArray = false;
      userDtos = [userDtos];
    }
    await Promise.all(
      userDtos.map(async userDto => {
        await this.passwordPreprocess(userDto);
      }),
    );
    if (isArray) {
      return userDtos;
    } else {
      return userDtos[0];
    }
  }

  private async passwordPreprocess(userDto: UserDto) {
    await this.validatePassword(userDto);
    await this.hashPassword(userDto);
  }

  private async validatePassword(userDto: UserDto): Promise<void> {
    const regExp = new RegExp('^(?=.*?[a-z])(?=.*?\\d)[a-z\\d]{8,100}$', 'i');
    if (!regExp.test(userDto.password)) {
      const message = 'passwordは英数字混合で8文字以上に設定してください';
      throw new BadRequestException(message);
    }
  }

  private async hashPassword(userDto: UserDto): Promise<void> {
    userDto.password = await getHash(userDto.password);
  }
}
