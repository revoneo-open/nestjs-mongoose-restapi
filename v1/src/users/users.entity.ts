/* tslint:disable:max-classes-per-file */
import { ApiModelProperty } from '@nestjs/swagger';
import { Document, Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import { Exclude } from 'class-transformer';

import {
  EntityBase,
  DateWise,
  ResponseBase,
  nameSchema,
  codeSchema,
  isClosedSchema,
  deletedAtSchema,
  timestamps,
} from '../app.entity';

export const UserSchema = new Schema(
  {
    ...nameSchema,
    ...codeSchema,
    ...isClosedSchema,
    password: {
      type: Schema.Types.String,
      validate: {
        validator: (v: string) => v.length === 60,
        message: 'not hashed password is provided.',
      } as any,
      required: [true, 'password is required.'],
    },
    is_admin: { type: Schema.Types.Boolean, default: false },
    can_view_limited: { type: Schema.Types.Boolean, default: false },
    ...deletedAtSchema,
  },
  timestamps,
).plugin(uniqueValidator, { message: '{PATH}が重複したuserが存在します' });

interface UserBase extends EntityBase {
  password: string;
  is_admin: boolean;
  can_view_limited: boolean;
}

export type User = UserBase & DateWise;

export type UserDocument = User & Document;

export class ResponseUserDto extends ResponseBase<UserDocument> {
  @Exclude()
  password?: string;

  @ApiModelProperty()
  is_admin?: boolean;

  @ApiModelProperty()
  can_view_limited?: boolean;
}

export class ResponseUsersDto {
  @ApiModelProperty()
  data: ResponseUserDto[];

  @ApiModelProperty()
  total: number;

  constructor(items: UserDocument[], total: number) {
    this.data = items.map(item => new ResponseUserDto(item));
    this.total = total;
  }
}

export class CreateUserDto implements UserBase {
  @ApiModelProperty()
  readonly name: string = '';

  @ApiModelProperty()
  readonly code: string = '';

  @ApiModelProperty()
  readonly is_closed: boolean = false;

  @ApiModelProperty()
  password: string = ''; // not readonly to hash

  @ApiModelProperty()
  readonly is_admin: boolean = false;

  @ApiModelProperty()
  readonly can_view_limited: boolean = false;
}

export class UpdateUserDto extends CreateUserDto {}
