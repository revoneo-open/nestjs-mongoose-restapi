import { QueryBase, QueryRaw } from '../app.query';

const selectableFields: string[] = ['is_admin', 'can_view_limited'];

export class UserQuery extends QueryBase {
  constructor(item: QueryRaw) {
    super(item, selectableFields);
  }
}
