import {
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema } from 'mongoose';
import contextService from 'request-context';

import { QueryRaw } from '../app.query';
import {
  create,
  createBulk,
  findAll,
  countAll,
  findOne,
  update,
  updateMany,
  softDelete,
  softDeleteMany,
  hardDelete,
} from '../app.model';
import { UserDocument, CreateUserDto, UpdateUserDto } from './users.entity';
import { UserQuery } from './users.query';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<UserDocument>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserDocument> {
    return await create<CreateUserDto, UserDocument>(
      createUserDto,
      this.userModel,
    );
  }

  async createBulk(createUserDtos: CreateUserDto[]): Promise<UserDocument[]> {
    return await createBulk<CreateUserDto, UserDocument>(
      createUserDtos,
      this.userModel,
    );
  }

  async findAll(query: QueryRaw): Promise<UserDocument[]> {
    return await findAll<UserQuery, UserDocument>(
      new UserQuery(query),
      this.userModel,
    );
  }

  async countAll(query: QueryRaw = {}): Promise<number> {
    return await countAll<UserQuery, UserDocument>(
      new UserQuery(query),
      this.userModel,
    );
  }

  async findOne(id: Schema.Types.ObjectId): Promise<UserDocument> {
    return await findOne<UserDocument>(id, this.userModel);
  }

  async update(
    id: Schema.Types.ObjectId,
    updateUserDto: UpdateUserDto,
  ): Promise<UserDocument> {
    return await update<UpdateUserDto, UserDocument>(
      id,
      updateUserDto,
      this.userModel,
    );
  }

  async updateMany(
    filter: string,
    updateUserDto: UpdateUserDto,
  ): Promise<Schema.Types.ObjectId[]> {
    return await updateMany<UpdateUserDto, UserDocument>(
      filter,
      updateUserDto,
      this.userModel,
    );
  }

  async softDelete(id: Schema.Types.ObjectId): Promise<UserDocument> {
    return await softDelete<UserDocument>(id, this.userModel);
  }

  async softDeleteMany(filter: {
    ids: Schema.Types.ObjectId[];
  }): Promise<Schema.Types.ObjectId[]> {
    return await softDeleteMany<UserDocument>(filter, this.userModel);
  }

  async hardDelete(id: Schema.Types.ObjectId): Promise<UserDocument> {
    return await hardDelete<UserDocument>(id, this.userModel);
  }

  // additional methods
  async findOneByName(name: string): Promise<UserDocument> {
    let item: UserDocument | null;
    try {
      item = await this.userModel.findOne({ name }).exec();
    } catch (e) {
      throw new InternalServerErrorException();
    }
    if (item === null) {
      throw new NotFoundException();
    }
    return item;
  }

  async findMe(id: Schema.Types.ObjectId): Promise<UserDocument> {
    const item = await this.findOne(id);
    if (item.id !== contextService.get('userId')) {
      throw new NotFoundException();
    }
    return item;
  }
}
