import {
  Controller,
  Get,
  Query,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ClassSerializerInterceptor,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiUseTags,
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiCreatedResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import { Schema } from 'mongoose';
import contextService from 'request-context';

import { apiDecorators } from '../app.constants';
import { ResponseIdsDto, ResponseDeletedDto } from '../app.entity';
import { QueryRaw } from '../app.query';
import { Roles } from '../decorators/roles.decorator';
import { PasswordHashPipe } from '../pipes/password-hash.pipe';
import {
  UserDocument,
  ResponseUserDto,
  CreateUserDto,
  UpdateUserDto,
  ResponseUsersDto,
} from './users.entity';
import { UsersService } from './users.service';

@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
@ApiUseTags('users')
@ApiBearerAuth()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @Roles('user')
  @UseGuards(AuthGuard('jwt'))
  @ApiCreatedResponse({ type: ResponseUserDto })
  async create(@Body(new PasswordHashPipe()) createUserDto: CreateUserDto) {
    return new ResponseUserDto(await this.usersService.create(createUserDto));
  }

  @Post('bulk')
  @Roles('user')
  @UseGuards(AuthGuard('jwt'))
  @ApiCreatedResponse({ type: ResponseUsersDto })
  async createBulk(
    @Body(new PasswordHashPipe()) createUserDtos: CreateUserDto[],
  ) {
    return new ResponseUsersDto(
      await this.usersService.createBulk(createUserDtos),
      await this.usersService.countAll(),
    );
  }

  @Get()
  @Roles('admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitQuery(apiDecorators.implicitQuery.limit)
  @ApiImplicitQuery(apiDecorators.implicitQuery.page)
  @ApiImplicitQuery(apiDecorators.implicitQuery.sort)
  @ApiImplicitQuery(apiDecorators.implicitQuery.or)
  @ApiImplicitQuery(apiDecorators.implicitQuery.filter)
  @ApiImplicitQuery(apiDecorators.implicitQuery.select)
  @ApiOkResponse({ type: ResponseUsersDto })
  async findAll(@Query() query: QueryRaw) {
    return new ResponseUsersDto(
      await this.usersService.findAll(query),
      await this.usersService.countAll(query),
    );
  }

  @Get(':id')
  @Roles('me', 'admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitParam(apiDecorators.implicitParam.id)
  @ApiOkResponse({ type: ResponseUserDto })
  async findOne(@Param('id') id: Schema.Types.ObjectId) {
    if (contextService.get('isAdminAccess')) {
      return new ResponseUserDto(await this.usersService.findOne(id));
    } else {
      return new ResponseUserDto(await this.usersService.findMe(id));
    }
  }

  @Put(':id')
  @Roles('me', 'admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitParam(apiDecorators.implicitParam.id)
  @ApiOkResponse({ type: ResponseUserDto })
  async update(
    @Param('id') id: Schema.Types.ObjectId,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return new ResponseUserDto(
      await this.usersService.update(id, updateUserDto),
    );
  }

  @Put()
  @Roles('admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitQuery(apiDecorators.implicitQuery.filterIds)
  @ApiOkResponse({ type: ResponseIdsDto })
  async updateMany(
    @Query('filter') filter: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return new ResponseIdsDto(
      await this.usersService.updateMany(filter, updateUserDto),
    );
  }

  @Delete(':id')
  @Roles('me', 'admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitParam(apiDecorators.implicitParam.id)
  @ApiOkResponse({ type: ResponseUserDto })
  async softDelete(@Param('id') id: Schema.Types.ObjectId) {
    return new ResponseDeletedDto<UserDocument>(
      await this.usersService.softDelete(id),
    );
  }

  @Delete()
  @Roles('admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitQuery(apiDecorators.implicitQuery.filterIds)
  @ApiOkResponse({ type: ResponseIdsDto })
  async softDeleteMany(@Query('filter')
  filter: {
    ids: Schema.Types.ObjectId[];
  }) {
    return new ResponseIdsDto(await this.usersService.softDeleteMany(filter));
  }

  @Delete('hard/:id')
  @Roles('admin')
  @UseGuards(AuthGuard('jwt'))
  @ApiImplicitParam(apiDecorators.implicitParam.id)
  @ApiOkResponse({ type: ResponseUserDto })
  async hardDelete(@Param('id') id: Schema.Types.ObjectId) {
    return new ResponseDeletedDto<UserDocument>(
      await this.usersService.hardDelete(id),
    );
  }
}
