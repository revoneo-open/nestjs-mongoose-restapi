import {
  Controller,
  Req,
  Post,
  Get,
  ClassSerializerInterceptor,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiUseTags,
  ApiBearerAuth,
  ApiImplicitQuery,
  ApiOkResponse,
} from '@nestjs/swagger';
import { Request } from 'express';

import { Roles } from '../decorators/roles.decorator';
import { ResponseUserDto, UserDocument } from '../users/users.entity';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { ResponseAccessTokenDto } from './auth.entity';

@Controller('')
@UseInterceptors(ClassSerializerInterceptor)
@ApiUseTags('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  @Roles('user')
  @UseGuards(AuthGuard('local'))
  @ApiImplicitQuery({ name: 'password', required: true })
  @ApiImplicitQuery({ name: 'username', required: true })
  @ApiOkResponse({ type: ResponseAccessTokenDto })
  async login(@Req() req: Request) {
    return this.authService.login(req.user as Partial<UserDocument>);
  }

  @Get('me')
  @Roles('user')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: ResponseUserDto })
  async getProfile(@Req() req: Request) {
    return new ResponseUserDto(
      await this.usersService.findOne((req.user as Partial<UserDocument>).id),
    );
  }
}
