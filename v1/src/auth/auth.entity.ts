import { ApiModelProperty } from '@nestjs/swagger';

export interface AuthPayload {
  id: string;
  username: string;
  is_admin: boolean;
}

export interface AccessToken {
  access_token: string;
}

export class ResponseAccessTokenDto implements AccessToken {
  @ApiModelProperty()
  access_token: string = '';
}
