import bcrypt from 'bcrypt';

const saltRounds = 10;

export const getHash = (plainText: string): Promise<string> => {
  return bcrypt.hash(plainText, saltRounds);
};

export const compare = (plainText: string, hash: string): Promise<boolean> => {
  return bcrypt.compare(plainText, hash);
};
