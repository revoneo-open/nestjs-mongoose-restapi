import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { AuthPayload, AccessToken } from './auth.entity';
import { compare } from './auth.bcrypt';
import { UsersService } from '../users/users.service';
import { UserDocument, ResponseUserDto } from '../users/users.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(
    name: string,
    pass: string,
  ): Promise<ResponseUserDto | null> {
    try {
      const user: UserDocument = await this.usersService.findOneByName(name);
      if (user && (await compare(pass, user.password))) {
        return new ResponseUserDto(user);
      }
      return null;
    } catch (e) {
      return null;
    }
  }

  login(user: Partial<UserDocument>): AccessToken {
    const payload: AuthPayload = {
      id: user.id,
      username: user.name || '',
      is_admin: user.is_admin || false,
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
