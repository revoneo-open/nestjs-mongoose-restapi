import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import contextService from 'request-context';

import { jwtConstants } from '../app.constants';
import { AuthPayload } from './auth.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  validate(payload: AuthPayload): AuthPayload | false {
    if (!this.validateAdmin(payload)) {
      return false;
    }
    return {
      id: payload.id,
      username: payload.username,
      is_admin: payload.is_admin,
    };
  }

  private validateAdmin(payload: AuthPayload): boolean {
    contextService.set('userId', payload.id);
    contextService.set('isAdminAccess', payload.is_admin);
    const roles: string[] = contextService.get('roles');
    if (roles === undefined) {
      return false;
    }
    if (roles.indexOf('user') > -1) {
      return true; // role:user, any loggined user can access
    }
    if (roles.indexOf('me') > -1) {
      return true; // role:me, a loggined user can access user own data
    }
    if (roles.indexOf('admin') > -1) {
      if (payload.is_admin === true) {
        return true; // role:admin only admin can access
      }
    }
    return false;
  }
}
