import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { mongodb } from './app.constants';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    MongooseModule.forRoot(mongodb, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
    AuthModule,
    UsersModule,
  ],
})
export class AppModule {}
