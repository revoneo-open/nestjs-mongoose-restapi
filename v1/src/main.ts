import { NestFactory, Reflector } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import contextService from 'request-context';
import i18n from 'i18n';
import helmet from 'helmet';

import { AppModule } from './app.module';
import { RolesGuard } from './guards/roles.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalGuards(new RolesGuard(app.get(Reflector)));

  app.use(contextService.middleware('request'));

  i18n.configure({
    locales: ['ja', 'en'],
    defaultLocale: 'ja',
    directory: __dirname.split('/dist')[0] + '/locales',
  });
  app.use(i18n.init);

  app.use(helmet());

  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('The Pigmalion API')
    .setDescription('The Pigmalion API description')
    .setVersion('1.0')
    .setBasePath('/v1')
    .addTag('auth')
    .addTag('users')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();
